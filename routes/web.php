<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@dashboard');

Route::get('refresh', function () {
    if (!empty($_SERVER['HTTP_X_APPENGINE_CRON']) || env('APP_ENV') !== 'production') {
        \Illuminate\Support\Facades\Artisan::call('playlist:generate');
        return response('OK', 200);
    } else {
        return response('Nope', 403);
    }
 });

Route::get('login/spotify', 'Auth\SpotifyController@redirectToProvider');
Route::get('login/spotify/callback', 'Auth\SpotifyController@handleProviderCallback');