# Scene Search #

Generates a CSV of tracks for bands playing soon in Lower Manhattan and Brooklyn.

### Setup ###

1. Clone the repository
2. Run `composer install` in the application's root

### Usage ###

- Execute `php artisan playlist:generate` 
- Open playlist.csv in the root of the application

The results of `playlist:generate` are saved in a local database. So if 
it breaks due to a connection issue you can rerun it and it will mostly
pick up where it left off.

### Other Commands ###

`php artisan band:ignore BAND_NAME` - Disables a band so it no longer shows up in the generated csv.

`php artisan playlist:export` - Writes the playlist.csv without refreshing the data from external sources. Useful after running `band:ignore`.