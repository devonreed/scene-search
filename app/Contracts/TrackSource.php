<?php

namespace App\Contracts;

use App\Models\Band;

interface TrackSource
{
    public function importTopTrack(Band $band);
}
