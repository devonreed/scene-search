<?php

namespace App\Sources\Shows;

use App\Contracts\ShowSource;
use App\Models\Band;
use Carbon\Carbon;

class TicketFly implements ShowSource
{
    const VENUES = [
        [
            'name' => 'Arlene\'s Grocery',
            'url' => 'https://www.ticketfly.com/venue/2545-arlenes-grocery/',
        ],
        [
            'name' => 'Rockwood Stage 2',
            'url' => 'https://www.ticketfly.com/venue/11239-rockwood-music-hall-stage-2/',
        ],
        [
            'name' => 'Rockwood Stage 3',
            'url' => 'https://www.ticketfly.com/venue/11241-rockwood-music-hall-stage-3/',
        ],
        [
            'name' => 'Bowery Electric Map Room',
            'url' => 'https://www.ticketfly.com/venue/25779-the-map-room-at-the-bowery-electric/',
        ],
    ];

    /**
     * Imports shows for all venues
     */
    public function import()
    {
        foreach (self::VENUES as $venue) {
            $this->importFromVenue($venue);
        }
    }

    /**
     * Imports all the shows for a venue
     *
     * @param $venue
     */
    protected function importFromVenue($venue)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $venue['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $page = curl_exec($ch);
        curl_close($ch);

        $shows = $this->parsePage($page);
        foreach ($shows as $show) {
            $this->importShow($venue, $show);
        }
    }

    /**
     * Imports show and band data into the database
     *
     * @param $venue
     * @param $show
     */
    protected function importShow($venue, $show)
    {
        $newShow = [
            'venue' => $venue['name'],
            'show_date' => new Carbon($show->startDate),
        ];

        $bands = array_map('trim', explode(',', $show->name));
        foreach ($bands as $bandName) {
            $band = Band::findOrCreate($bandName);
            if (!$band->isDuplicateShow($venue['name'])) {
                $band->shows()->create($newShow);
            }
        }
    }

    /**
     * Parses the show data from an html page
     *
     * @param $page
     * @return mixed|null
     */
    protected function parsePage($page)
    {
        libxml_use_internal_errors(true); // Allow imperfect HTML

        $doc = new \DOMDocument();
        $doc->loadHTML($page, LIBXML_NOWARNING);
        $scripts = $doc->getElementsByTagName('script');
        foreach ($scripts as $script) {
            if ($script->getAttribute('type') === 'application/ld+json') {
                return json_decode($script->nodeValue);
            }
        }

        return null;
    }
}
