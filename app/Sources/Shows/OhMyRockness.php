<?php

namespace App\Sources\Shows;

use App\Contracts\ShowSource;
use App\Models\Band;
use Carbon\Carbon;

class OhMyRockness implements ShowSource
{
    const VENUES = [
        [
            'name' => 'Mercury Lounge',
            'url' => 'https://www.ohmyrockness.com/venues/mercury-lounge',
        ],
        [
            'name' => 'Berlin',
            'url' => 'https://www.ohmyrockness.com/venues/berlin',
        ],
        [
            'name' => 'Bowery Electric',
            'url' => 'https://www.ohmyrockness.com/venues/the-bowery-electric',
        ],
    ];

    /**
     * Imports shows for all venues
     */
    public function import()
    {
        foreach (self::VENUES as $venue) {
            try {
                $this->importFromVenue($venue);
            } catch (\Exception $e) {
                \dd($e);
                die();
            }
        }
    }

    /**
     * Imports all the shows for a venue
     *
     * @param $venue
     */
    protected function importFromVenue($venue)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $venue['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $page = curl_exec($ch);
        curl_close($ch);

        $shows = $this->parsePage($page);
        if (count($shows) === 0) {
            echo "No shows for " . $venue['name'];
        }
        foreach ($shows as $show) {
            $this->importShow($venue, $show);
        }
    }

    /**
     * Imports show and band data into the database
     *
     * @param $venue
     * @param $show
     */
    protected function importShow($venue, $show)
    {
        $newShow = [
            'venue' => $venue['name'],
            'show_date' => new Carbon($show->startDate),
        ];

        $bands = array_map('trim', explode(',', $show->name));
        foreach ($bands as $bandName) {
            $band = Band::findOrCreate($bandName);
            if (!$band->isDuplicateShow($venue['name'])) {
                $band->shows()->create($newShow);
            }
        }
    }

    /**
     * Parses the show data from an html page
     *
     * @param $page
     * @return mixed|null
     */
    protected function parsePage($page)
    {
        libxml_use_internal_errors(true); // Allow imperfect HTML

        $doc = new \DOMDocument();
        $doc->loadHTML($page, LIBXML_NOWARNING);
        $scripts = $doc->getElementsByTagName('script');
        foreach ($scripts as $script) {
            if ($script->getAttribute('type') === 'application/ld+json') {
                return json_decode($script->nodeValue);
            }
        }

        return [];
    }
}
