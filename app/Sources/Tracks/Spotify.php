<?php

namespace App\Sources\Tracks;

use App\Contracts\TrackSource;
use App\Models\Band;
use Carbon\Carbon;

class Spotify implements TrackSource
{
    const SEARCH_API_URL = 'https://api.spotify.com/v1/search?q={search}&type=artist';

    const TRACK_API_URL = 'https://api.spotify.com/v1/artists/{id}/top-tracks?country=US';

    const PLAYLIST_API_URL = 'https://api.spotify.com/v1/users/1265280079/playlists/0qG6Tm6bnPOhbWm1FVFUhi/tracks';

    private $token;

    public function __construct()
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, 'https://accounts.spotify.com/api/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=refresh_token&refresh_token=AQBe_coGT1xieKmGCdXjD_FUr72NZHeisTZfs7K5Xq1n6-MQFmR6wV0t0gpmROk664UWQk-XRAsKBf53NCgeYbhj7C0nlZF4i0feVt0FBf9caH4TG3maP6DOpZpncMB4Tl8');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MTM0MDIzNTUzZTJlNGUzOWEwOTBiZTE1NGVkMThkN2E6Njk4YjJlMTA5MmJhNGI4NmJmMmE2ZTk5MTRhYjllMDk='));
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        $this->token = $obj->access_token;
    }

    /**
     * Saves the most played track for a band to the database
     *
     * @param Band $band
     */
    public function importTopTrack(Band $band)
    {
        if ($band->ignore) {
            return;
        }

        // Use the existing top track if it isn't too stale
        if ($band->top_spotify_track && $band->updated_at > (new Carbon())->subDays(30)) {
            return;
        }

        $spotifyId = $this->getSpotifyId($band);
        if (!$spotifyId) {
            return;
        }

        $topTrack = $this->getTopTrack($spotifyId);
        if (!$topTrack) {
            return;
        }

        $band->top_spotify_track = $topTrack->id;
        $band->save();
    }

    /**
     * Gets the top tracks for a user
     *
     * @param $spotifyId
     * @return obj
     */
    protected function getTopTrack($spotifyId)
    {
        $searchUrl = str_replace('{id}', $spotifyId, self::TRACK_API_URL);

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $searchUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $this->token));
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        if (!empty($obj->tracks)) {
            return $obj->tracks[0];
        }
    }

    protected function getSpotifyId(Band $band)
    {
        if (!$band->spotify_id && !$band->spotify_search_failed) {
            $this->searchForBandId($band);
        }

        return $band->spotify_id;
    }

    /**
     * Search Spotify for a band. Adds the Spotify id to the band
     * if found.
     *
     * @param Band $band
     */
    protected function searchForBandId(Band $band)
    {
        $searchUrl = str_replace('{search}', urlencode($band->name), self::SEARCH_API_URL);
        
        try {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $searchUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $this->token));
            $result = curl_exec($ch);
            curl_close($ch);

            $obj = json_decode($result);
            if (!empty($obj->artists->items)) {
                $artist = $obj->artists->items[0];
                $band->spotify_id = $artist->id;
            } else {
                $band->spotify_search_failed = true;
            }
        } catch (\Exception $e) {
            $band->spotify_search_failed = true;
        }
            
        $band->save();
    }

    public function replacePlaylist(array $ids)
    {
        $counter = 0;
        while ($counter < count($ids)) {
            $slice = array_slice($ids, $counter, 100);
            foreach ($slice as $index => $id) {
                $slice[$index] = 'spotify:track:' . $id;
            }
            $data = json_encode(['uris' => $slice]);
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, self::PLAYLIST_API_URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $this->token,
                'Content-Type: application/json',
            ));
            if ($counter === 0) {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
            $result = curl_exec($ch);
            curl_close($ch);

            $counter += 100;
        }
    }
}
