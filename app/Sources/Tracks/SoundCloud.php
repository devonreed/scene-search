<?php

namespace App\Sources\Tracks;

use App\Contracts\TrackSource;
use App\Models\Band;
use Carbon\Carbon;

class SoundCloud implements TrackSource
{
    const SEARCH_API_URL = 'https://api-v2.soundcloud.com/search?q={search}&facet=model&user_id=162938-611767-625797-643039&client_id=lxLfZovZidDkh53DlbOsnWQNRNKjfSo5&limit=10&offset=0&linked_partitioning=1&app_version=1506946076';

    const TRACK_API_URL = 'https://api-v2.soundcloud.com/users/{user_id}/tracks?representation=&client_id=lxLfZovZidDkh53DlbOsnWQNRNKjfSo5&limit=20&offset=0&linked_partitioning=1&app_version=1506946076';

    /**
     * Saves the most played track for a band to the database
     *
     * @param Band $band
     * @return mixed|null
     */
    public function importTopTrack(Band $band)
    {
        if ($band->ignore) {
            return null;
        }

        // Use the existing top track if it isn't too stale
        if ($band->top_track && $band->updated_at > (new Carbon())->subDays(30)) {
            return null;
        }

        $soundCloudUserId = $this->getSoundCloudUserId($band);
        if (!$soundCloudUserId) {
            return null;
        }

        $tracks = $this->getRecentTracks($soundCloudUserId);
        if (count($tracks) === 0) {
            return null;
        }

        $topTrack = null;
        foreach ($tracks as $track) {
            if (!$topTrack) {
                $topTrack = $track;
                continue;
            }

            if ($topTrack->playback_count < $track->playback_count) {
                $topTrack = $track;
            }
        }

        $band->top_track = [
            'title' => $topTrack->title,
            'url' => $topTrack->permalink_url,
        ];
        $band->save();
    }

    /**
     * Gets the 20 most recent tracks for a user
     *
     * @param $userId
     * @return array
     */
    protected function getRecentTracks($userId)
    {
        $trackAPI = str_replace('{user_id}', $userId, self::TRACK_API_URL);
        $result = file_get_contents($trackAPI);
        try {
            $tracks = json_decode($result);
            return $tracks->collection;
        } catch (\Exception $e) {
            return [];
        }
    }

    protected function getSoundCloudUserId(Band $band)
    {
        if (!$band->soundcloud_user_id && !$band->soundcloud_search_failed) {
            $this->searchForBandUserId($band);
        }

        return $band->soundcloud_user_id;
    }

    /**
     * Search SoundCloud for a band. Adds the user id to the band
     * if found.
     *
     * @param Band $band
     */
    protected function searchForBandUserId(Band $band)
    {
        $searchUrl = str_replace('{search}', urlencode($band->name), self::SEARCH_API_URL);
        $result = file_get_contents($searchUrl);

        try {
            $json = json_decode($result);
            $matches = ['NY', 'New York', 'Brooklyn'];

            $users = [];

            foreach ($json->collection as $item) {
                if ($item->kind === 'user') {
                    $users[] = $item;
                    if (preg_match('/'.implode('|', $matches).'/', $item->city)) {
                        $band->soundcloud_user_id = $item->id;
                        $band->save();
                        break;
                    }
                }
            }

            if (!$band->soundcloud_user_id) {
                if (!empty($users)) {
                    $band->soundcloud_user_id = $users[0]->id;
                } else {
                    $band->soundcloud_search_failed = true;
                }
                $band->save();
            }
        } catch (\Exception $e) {
            $band->soundcloud_search_failed = true;
            $band->save();
        }
    }
}
