<?php

namespace App\Console\Commands;

use App\Models\Band;
use Illuminate\Console\Command;

class IgnoreBand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'band:ignore {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ignores a band so it no longer shows up in the generated csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bandName = $this->argument('name');
        $band = Band::where('name', $bandName)->first();
        if (!$band) {
            $this->error("Unable to find $bandName");
            exit(1);
        }

        $band->ignore = true;
        $band->save();
        $this->info("$bandName ignored");
    }
}
