<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Sources\Tracks\Spotify;

class ExportPlaylist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'playlist:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Writes the playlist data to a csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Creating csv');
        $items = DB::table('shows')
            ->join('bands', 'shows.band_id', '=', 'bands.id')
            ->whereNotNull('top_track')
            ->where('ignore', false)
            ->where('show_date', '>', (new Carbon())->subDays(30))
            ->orderBy('shows.created_at', 'desc')
            ->get();

        /*
        $fp = fopen('playlist.csv', 'w');
        fputcsv($fp, ['Band', 'Track Title', 'Track Url', 'Next Show Date', 'Next Show Venue']);
        foreach ($items as $item) {
            $track = json_decode($item->top_track);
            fputcsv($fp, [
                $item->name,
                $track->title,
                $track->url,
                $item->show_date,
                $item->venue,
                $item->created_at,
            ]);
        }
        fclose($fp);
        $this->info('playlist.csv generated');
        */

        $items = DB::table('shows')
            ->where('shows.show_date', Carbon::now()->toDateString())
            ->join('bands', 'bands.id', '=', 'shows.band_id')
            ->whereNotNull('bands.top_spotify_track')
            ->where('bands.ignore', false)
            ->get();

        $spotify_ids = $items->pluck('top_spotify_track');
        $spotify = new Spotify();
        $spotify->replacePlaylist($spotify_ids->all());
        $this->info('Spotify playlist refreshed');
    }
}
