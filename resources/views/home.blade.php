<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Custom NYC Spotify Playlists</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="description" content="Custom NYC Spotify Playlists Updated Daily"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta itemprop="name" content="Custom NYC Spotify Playlists">
    <meta itemprop="description" content="Custom NYC venue playlists on Spotify, updated daily">
	
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}" />
	
	<style>
		.hidden, ul.actions li.hidden { 
			display: none;
		}
	</style>
</head>
<body>
	<section id="header">
		<div id="search" class="inner">
			<h2>Connect to Spofify</h2>
			<p id="infotext">Click the login button to connect to grant access to create a playlist on your Spotify account.</p>
			<ul class="actions">
				<li id="upload">
					<button id="uploadbtn">Login</button>
				</li>
			</ul>
		</div>
	</section>
</body>
</html>